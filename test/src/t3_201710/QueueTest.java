package t3_201710;

import junit.framework.TestCase;
import model.data_structures.Queue;

public class QueueTest extends TestCase{
	
	Queue<String> cola;
	
	public void escenario1()
	{
		cola = new Queue<String>();
	}
	public void escenario2()
	{
		escenario1();
		cola.enqueue("Primer elemento");
		cola.enqueue("Segundo elemento");
		cola.enqueue("Tercer elemento");
		
	}
	
	public void testEnqueue()
	{
		escenario2();
		assertEquals( "La cantidad de elementos no coincide con la cantidad agregada", 3, cola.size() );
	}
	
	public void testDequeue()
	{
		escenario2();
		assertEquals( "El elemento eliminado no coincide", "Primer elemento", cola.dequeue() );
	}

}
