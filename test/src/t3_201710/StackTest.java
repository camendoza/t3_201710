package t3_201710;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class StackTest extends TestCase{
	
	private Stack<String> pila;
	
	public void escenario1()
	{
		pila = new Stack<>();
	}
	
	public void escenario2()
	{
		escenario1();
		pila.push("Primer elemento");
		pila.push("Segundo elemento");
		pila.push("Tercer elemento");
	}
	public void escenario3()
	{
		escenario2();
		pila.pop();
		
	}
	
	public void testPush()
	{
	 escenario2();
     assertEquals( "La cantidad de elementos no coincide con la cantidad agregada", 3, pila.size() );
     
	}
	
	public void testPop()
	{
		escenario3();
		//escenario2();
		//assertEquals( "El elemento eliminado no corresponde", "Tercer elemento", pila.pop() );
		assertEquals( "La cantidad de elementos no disminuyo", 2, pila.size() );
	}

}
