package controller;
import model.data_structures.Stack;
import model.logic.ClaseLogica;


public class Controller {
private static ClaseLogica<Character> model = new ClaseLogica<Character>();

public static boolean expresionBienFormada(String exp)
{
	return model.expresionBienFormada(exp);
	
}
public static Stack<Character> ordenarPila(Stack<Character> Pila)
{
	 
	 return model.ordenarPila(Pila);
}
public static Stack<Character> cargarFormula(String formula)
{
	return model.cargarFormula(formula);
}

	
	
	
	

}
