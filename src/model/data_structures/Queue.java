package model.data_structures;

public class Queue<T> {
	
	private ILista<T> lista;
	private NodoSencillo<T> primero;
	private NodoSencillo<T> ultimo;
	private int size;
	
	public Queue()
	{
		lista = new ListaEncadenada<T>();
		size=0;
	}
	public void enqueue(T item)
	{
		NodoSencillo<T> nuevo = new NodoSencillo<T>(item);
		NodoSencillo<T> viejoUltimo = ultimo;
		ultimo = nuevo;
		if(isEmpty())
		{
			primero = ultimo;
		}
			
		else
	    {
			viejoUltimo.cambiarSiguiente(ultimo);
		}
		lista.agregarElementoFinal(ultimo.darItem());
		
		size++;
	}
	public T dequeue()
	{
		T item = primero.darItem();  
	    primero = primero.darSiguiente();
	    if(isEmpty())
	    {
	    	ultimo = null;
	    }
	    size--;     
	    return item; 
	}
	public boolean isEmpty()
	{
		return size==0;
		
	}
	public int size()
	{
		return size;
	}
	
	
}
