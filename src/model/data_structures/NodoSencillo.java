package model.data_structures;

public class NodoSencillo<E>
{
	private NodoSencillo<E> siguiente;
	private E item;
	
	public NodoSencillo(E item) 
	{
		siguiente = null;
		this.item = item;
	}
	
	public NodoSencillo<E> darSiguiente()
	{
		return siguiente;
	}
	
	public void cambiarSiguiente(NodoSencillo<E> nuevoSiguiente)
	{
		siguiente= nuevoSiguiente;
	}
	
	public E darItem()
	{
		return item;
	}
	
	public void cambiarItem (E nuevoItem)
	{
		item = nuevoItem;
	}
	
	
 
}

