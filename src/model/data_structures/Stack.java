package model.data_structures;

public class Stack<T>{
	
		private ILista<T> lista;
	    private int size;
		private NodoSencillo<T> primero;



public Stack()
{
  lista = new ListaEncadenada<T>();
  primero = null;
  size=0;
}
public void push(T item)
{
	NodoSencillo<T> nuevo = new NodoSencillo<T>(item);
	NodoSencillo<T> antiguoPrimero = primero;
	primero = nuevo;
	primero.cambiarSiguiente(antiguoPrimero);
	lista.agregarElementoFinal(primero.darItem());
	size++;
	
}

public T pop()
{
	T temp = primero.darItem();
	
	
    primero = primero.darSiguiente();
	
    size--;      
    return temp;
}
    

	


public boolean isEmpty()
{
	return size==0;
	
}

public int size()
{
	return size;
}


}