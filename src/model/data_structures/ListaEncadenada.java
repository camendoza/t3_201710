package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private int size;
	private NodoSencillo<T> cabeza;
	private NodoSencillo<T> actual;

	public ListaEncadenada() {
		cabeza = null;
		actual = cabeza;
		size = 0;
	}

	public Iterator<T> iterator() {

		return new Iterator<T>() {
			NodoSencillo<T> actual = null;

			@Override
			public boolean hasNext() {

				if (actual == null)
					return cabeza.darItem() != null;
				else
					return actual.darSiguiente() != null;

			}

			@Override
			public T next() {
				if (actual == null) {
					actual = cabeza;
					if (actual == null)
						return null;
					else
						return actual.darItem();
				}

				else
					actual = actual.darSiguiente();

				return actual.darItem();
			}

		};

	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoSencillo<T> nuevo = new NodoSencillo<T>(elem);
		if (cabeza == null) {
			cabeza = nuevo;
			actual = cabeza;
			size++;
		} else {
			NodoSencillo<T> actual = cabeza;
			while (actual.darSiguiente() != null)

			{
				actual = actual.darSiguiente();
			}
			actual.cambiarSiguiente(nuevo);
			size++;
		}
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		if (pos == 0) {
			return cabeza.darItem();
		}
		int contador = 0;
		NodoSencillo<T> actual = cabeza;
		while (actual.darSiguiente() != null) {
			actual = actual.darSiguiente();
			contador++;

			if (contador == pos) {
				return actual.darItem();
			}
		}
		return null;

	}

	@Override
	public int darNumeroElementos() {

		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		if (actual == null)
			return null;

		else
			return actual.darItem();

	}

	@Override
	public boolean avanzarSiguientePosicion() {

		if (actual != null) {
			if (actual.darSiguiente() != null) {
				actual = actual.darSiguiente();
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean retrocederPosicionAnterior() {

		NodoSencillo<T> anterior = null;
		NodoSencillo<T> act2 = cabeza;
		if (act2 != null) {
			while (act2.darSiguiente() != null && !act2.equals(actual)) {
				anterior = act2;
				act2 = act2.darSiguiente();
			}
			if (act2.equals(actual)) {
				actual = anterior;
				return true;
			}

		}
		return false;
	}

}
