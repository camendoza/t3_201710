package model.logic;

import model.data_structures.Queue;
import model.data_structures.Stack;

public class ClaseLogica<T> {

	public boolean expresionBienFormada(String expresion)
	{
		
		char[] caracteres= expresion.toCharArray();
		Stack<Character> pilaNueva = new Stack<Character>();
		try{
		for(int i=0; i< caracteres.length;i++)
		{
			char act = caracteres[i];
			if(act == '[' || act == '(')
			{
				pilaNueva.push(act);
			}
		
			else if(act==')')
			{
			      if(pilaNueva.pop()!='(')
					{
						return false;
					}

			}
				
			else if (act==']') 
			{
					if(pilaNueva.pop()!='[')
				    {
						return false;
					}
			}
				
		}
		}
		catch(Exception e)
		{
			return false;
		}
		
	
		
	if(pilaNueva.isEmpty())

	{
		return true;
	}
	
	return false;
}

	public Stack<T> ordenarPila(Stack<T> pila)
	{
		Queue<Character> aux = new Queue<Character>();
		Stack<Character> temp = (Stack<Character>) pila;
		int tam = temp.size();
		int ind1=0;
		while(ind1<tam)
		{
			char act = (char)temp.pop();
			if(act=='('||act=='['||act==')'||act==']')
			{
				aux.enqueue(act);
				
			}
			else
			{
				temp.push(act);
				
			}
			
			ind1++;
		}
         int ind2 =0;
		while(ind2<temp.size())
		{
			char act = (char)temp.pop();
			if(act=='+'||act=='*'||act=='-'||act=='/')
			{
				aux.enqueue(act);
			}
			else
			{
				temp.push(act);
			}
			
			ind2++;
		}
	
		int ind3 = 0;
		while(ind3<temp.size())
		{
			
			char act = (char)temp.pop();
			aux.enqueue(act);
			
			ind3++;
			
		}
		
		while(aux.size()!=0)
		{
			temp.push(aux.dequeue());
		}
		
		return (Stack<T>) temp;
		
	}

	public Stack<T> cargarFormula(String formula) {
	
		char[] caracteres = formula.toCharArray();
		Stack<Character> pila = new Stack<Character>();

		for (int i = 0; i < caracteres.length; i++) {
			char act = caracteres[i];
			pila.push(act);
		}

		return (Stack<T>) pila;
	}
	

}
