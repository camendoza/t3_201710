package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.Stack;

public class Vista {
	private static void printMenu(){
		System.out.println("1. Crear una nueva expresion");
		System.out.println("2. Expresion bien formada");
		System.out.println("3. Dar expresion ordenada");
		System.out.println("4. Salir");
	    System.out.println("Elegir una opcion");
	}
	private static String leerCadena()
	{
	try
	{
		String line = new Scanner(System.in).nextLine();
		return line;
	}
	catch(Exception e )
	{
		System.out.println("error");
		
	}
	return null;
}


	
	
	
	
	public static void main (String args[])
	{
		
		String formula = null;
		Stack<Character> pila = null;
		Scanner sc = new Scanner(System.in);
		
		for(;;){
			  printMenu();
			  int option = sc.nextInt();
		  switch(option){
			  case 1:System.out.println("--------- \n Ingrese la expresion  \n---------"); 
				  formula = leerCadena(); pila= Controller.cargarFormula(formula);System.out.println("--------- \n La expresion ha sido creada  \n---------");
			  break;
			  
			  case 2:
				  if(Controller.expresionBienFormada(formula)==true)
				  System.out.println("--------- \n La expresion esta bien escrita"+" \n---------");
				  else
					  System.out.println("--------- \n La expresion no esta bien escrita"+" \n---------");
					  
					  
			  break;
			  
			  case 3: System.out.println("--------- \n Se ordeno la expresion "+" \n---------");
			  
			  Stack<Character> ordenada = Controller.ordenarPila(pila);
			  
			  while(ordenada.size()>0)
			  {
				  System.out.println(ordenada.pop());
			  }
			  break;
			  
			  
			 
			  
			  case 4: System.out.println("¡Adios!  \n---------"); sc.close(); return;	
			  default: System.out.println("--------- \n Invalid option !! \n---------");
		  }
		}
	}
}

